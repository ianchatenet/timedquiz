const data = [
    {
        theme: "grecque",
        contentFr: "Qui est didier",
        contentEn: "Who's didier",
        difficulty: 2,
        options: [],
        answer: "oui",
        desc: "ouuuuuuuuuuuuuuuuuuuuuuuep super description"
    },
    {
        theme: "slave",
        contentFr: "Qui est bernard",
        contentEn: "Who's bernard",
        difficulty: 1,
        options: ["Merlin", "non", "oui", "non"],
        answer: "bernard",
        desc: "ouuuuuuuuuuuuuuuuuuuuuuuep super description"
    },
    {
        theme: "incas",
        contentFr: "Qui est Christ cosmique",
        contentEn: "Who's Christ cosmique",
        difficulty: 2,
        options: ["Merlin", "non"],
        answer: "Merlin",
        desc: "ouuuuuuuuuuuuuuuuuuuuuuuep super description"
    },
    {
        theme: "nordique",
        contentFr: "Qui est truc",
        contentEn: "Who's truc",
        difficulty: 3,
        options: ["bidule", "noeeeen"],
        answer: "bidule",
        desc: "ouuuuuuuuuuuuuuuuuuuuuuuep super description"
    }
]

export default data;