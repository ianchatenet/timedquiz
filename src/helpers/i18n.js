import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import app_fr from "../translations/fr/app.json"
import app_en from "../translations/en/app.json"
import home_fr from "../translations/fr/home.json"
import home_en from "../translations/en/home.json"
import quiz_fr from "../translations/fr/quiz.json"
import quiz_en from "../translations/en/quiz.json"
import profil_fr from "../translations/fr/profil.json"
import profil_en from "../translations/en/profil.json"
import authentication_fr from "../translations/fr/authentication.json"
import authentication_en from "../translations/en/authentication.json"
import admin_fr from "../translations/fr/admin.json"
import admin_en from "../translations/en/admin.json"
import modo_fr from "../translations/fr/modo.json"
import modo_en from '../translations/en/modo.json'
import ePages_fr from '../translations/fr/errorPages.json'
import ePages_en from '../translations/en/errorPages.json'
import confirm_fr from '../translations/fr/confirm.json'
import confirm_en from '../translations/en/confirm.json'


i18n.use(initReactI18next)
    .init({
        debug: false, //console
        lng: 'fr', //default
        fallbackLng: ["fr", "en"], //list languages
        resources: {
            fr: {
                app: app_fr,
                home: home_fr,
                quiz: quiz_fr,
                profil: profil_fr,
                authentication: authentication_fr,
                admin: admin_fr,
                modo:modo_fr,
                ePages:ePages_fr,
                confirm:confirm_fr
            },
            en: {
                app: app_en,
                home: home_en,
                quiz: quiz_en,
                profil: profil_en,
                authentication: authentication_en,
                admin: admin_en,
                modo:modo_en,
                ePages:ePages_en,
                confirm:confirm_en
            }
        },
    });

export default i18n;