import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/js/dist/modal';
import 'bootstrap/js/dist/collapse';
import './App.css';
import Navbar from './components/navbar/Navbar.js';
import Footer from './components/footer/Footer.js';
import { useDispatch } from 'react-redux'
import {changePage,login} from "./redux/actions"
import {getCookie} from "./cookie/cookies"

function App() {

let page = getCookie("page")
let user = getCookie("user")
const dispatch = useDispatch()
if(page)dispatch(changePage(page)) 
if(user)dispatch(login(JSON.parse(user))) 
 

  return (
    <div className="background">
      <Navbar />
      {/* <Footer /> */}
    </div>
  );
}

export default App;
