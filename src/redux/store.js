import {createStore, compose} from '@reduxjs/toolkit';
import Reducer from './reducers'

const initState = {
    user: false,
    page:"home",
}


const store = createStore(Reducer,initState,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
export default store 
