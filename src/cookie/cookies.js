

function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
  }

function setCookie(cookieName, value){
  let time=60*60;
  document.cookie = `${cookieName}=${value}; max-age=${time}`
}

function removeCookie(cookieName){
  document.cookie = `${cookieName}=""; max-age=0`
}

  export{getCookie,setCookie,removeCookie}