let baseUrlApi = 'https://127.0.0.1:8000/api/'
let baseUrlPublic ="https://127.0.0.1:8000/mytholoquiz/"

export const quiz={
    getQuiz:  ()=>  fetch(baseUrlApi+'quiz',{method:'GET'}),
    createGame: (user,questions,score)=> fetch(baseUrlApi+'createGame',
    {
        method:"POST",
        body: JSON.stringify({
            "user":user.id,
            "questions":questions,
            "score":score
        })
    }),

}

export const mytholo={
    getThemes: () => fetch(baseUrlPublic+'themes',{method:"GET"}),
}

export const profil = {

    login:(email,password)=>fetch(baseUrlApi+'login', {
        method: 'POST',
        body: JSON.stringify({
            "email":email,
            "password":password
        })
      }),
      refresh:(user)=>fetch(baseUrlApi+"profil/refresh",{
          method:"GET",
          headers: {
            "X-Auth-Token": user.apiToken,
            "Content-Type": "application/json"
          }
      }),

      logout:(email)=>fetch(baseUrlApi+'logout',{
        method:'POST',
        body: JSON.stringify({
            "email":email
        })
      }),

      update:(token,user)=>fetch(baseUrlApi+"profil/update",{
          method:'PUT',
          headers: {
            "X-Auth-Token": token,
            "Content-Type": "application/json"
          },
          body:JSON.stringify({
              "user":user
          })
      }),

      register:(username,email,password)=> fetch(baseUrlApi+"register",{
        method:"POST",
        body: JSON.stringify({
            "username":username,
            "email": email,
            "password":password
        })
    }),

    delete:(user)=> fetch(baseUrlApi+"profil/delete",{
        method:"DELETE",
        headers:{
            "X-Auth-Token": user.apiToken,
            "Content-Type": "application/json"
        },
        body:JSON.stringify({
            "user":user
        })
    })

};

export const modo={

    createQuestion:(token,question)=>fetch(baseUrlApi+"modo/question",{
        method:"POST",
        headers:{
            "X-Auth-Token": token,
            "Content-Type": "application/json"
        },
        body:JSON.stringify({
            "question":question
        })
    })
}

export const admin={
    //questions
    getQuestions:(token)=>fetch(baseUrlApi+"admin/questions",{
        method:"GET",
        headers:{
            "X-Auth-Token": token,
            "Content-Type": "application/json"
        },
    }),
    updateQuestion:(token,questions)=>fetch(baseUrlApi+"admin/question",{
        method:"PUT",
        headers:{
            "X-Auth-Token": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "questions":questions,
        })
    }),
    deleteQuestion:(token,questionId)=>fetch(baseUrlApi+"admin/question",{
            method:"DELETE",
            headers:{
                "X-Auth-Token": token,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "questionId":questionId
            })
        }),
    createQuestion:(user,question)=>fetch(baseUrlApi+"admin/question",{
            method:"POST",
            headers:{
                "X-Auth-Token": user.apiToken,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "question":question,
                "userId":user.id
            })
        }),
        //users
    getUsers:(token)=>fetch(baseUrlApi+"admin/users",{
        method:"GET",
        headers:{
            "X-Auth-Token": token,
            "Content-Type": "application/json"
        }
    }),
    updateUser:(token,user)=>fetch(baseUrlApi+"admin/user",{
        method:"PUT",
        headers:{
            "X-Auth-Token": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "user":user,
        })
    }),
    getThemes:(token)=>fetch(baseUrlApi+"admin/themes",{
        method:"GET",
        headers:{
            "X-Auth-Token": token,
            "Content-Type": "application/json"
        }
    }),
    createTheme:(token,theme)=>fetch(baseUrlApi+"admin/theme",{
        method:"POST",
        headers:{
            "X-Auth-Token": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "theme":theme,
        })
    }),
    updateTheme:(token,theme)=>fetch(baseUrlApi+"admin/theme",{
        method:"PUT",
        headers:{
            "X-Auth-Token": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "theme":theme,
        })
    }),
    deleteTheme:(token,theme)=>fetch(baseUrlApi+"admin/theme",{
        method:"DELETE",
        headers:{
            "X-Auth-Token": token,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "id":theme.id,
        })
    })

}