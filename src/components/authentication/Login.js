import React, { useState, Fragment } from 'react'
import { useTranslation } from "react-i18next";
import {profil} from "../../api";
import { useDispatch } from 'react-redux'
import {login,changePage} from "../../redux/actions"
import { setCookie } from '../../cookie/cookies';
import { Redirect } from 'react-router-dom';


export default function Login() {
    const dispatch = useDispatch()
    const { t } = useTranslation('authentication');

    const [logged, setLogged] = useState(false)
    const [errors, setErrors] = useState([]) //api request error
    return (
        <div>
            {logged===true ?
            <Redirect to="/"/>
            :
            <Fragment>
                <div className="col-12 shadow p-3 mb-5 bg-white rounded titleBar text-center">
                <h3>{t("login.title")}</h3>
            </div>
            <div className="shadow p-3 mb-5 bg-white rounded col-6 offset-3">
                <form onSubmit={handleSubmit}>

                
                        {errors.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{t(error)}</p>
                        )}
                    

                    <div className="form-group">
                        <label >{t("login.email.label")}</label>
                        <input type="text" className="form-control" id="email" placeholder={t("login.email.placeholder")}></input>
                    </div>

                    <div className="form-group">
                        <label >{t("login.password.label")}</label>
                        <input type="password" className="form-control" id="password" placeholder={t("login.password.placeholder")}></input>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>

            </Fragment>
            
            }
            
        </div>
    )

    async function handleSubmit(e) {
        e.preventDefault()
        let email = document.getElementById("email").value
        let password = document.getElementById("password").value
        let errorsArray=[];
        try {
            const response = await profil.login(email,password)
            const user = await response.json()
            if(user === "bad credentials"){
                errorsArray.push("login.errors.identi")
            }
            setErrors(errorsArray)
            if(errorsArray.length === 0){
                setLogged(true)
                dispatch(changePage("home"))
                dispatch(login(user))
                setCookie("user",JSON.stringify(user))

            }
            
          } catch (err) {
              setErrors([err.message])
          }
    }
}