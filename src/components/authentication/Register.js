import React, { useState } from 'react'
import { useTranslation } from "react-i18next";
import { useDispatch } from 'react-redux'
import { login, changePage } from "../../redux/actions"
import { setCookie } from '../../cookie/cookies';
import { profil } from "../../api"
import { Redirect } from 'react-router-dom';


function Register() {
    const dispatch = useDispatch()

    const { t } = useTranslation('authentication');
    const [errors, setErrors] = useState([])
    const [emailE, setEmailE] = useState([])
    const [nameE, setNameE] = useState([])
    const [passE, setPassE] = useState([])
    const [confirmE, setConfirmE] = useState([])

    return (
        <div>

            <div className="col-12 shadow p-3 mb-5 bg-white rounded titleBar text-center">
                <h3>{t("register.title")}</h3>
            </div>
            <div className="shadow p-3 mb-5 bg-white rounded col-6 offset-3">
                <form onSubmit={handleSubmit}>
                    {errors.map((error, index) =>
                        <p key={index} className="text-danger errorForm">{error}</p>
                    )}
                    <div className="form-group">
                        <label >{t("register.email.label")}</label>
                        {emailE.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{error}</p>
                        )}
                        <input type="text" className="form-control" id="email" placeholder={t("register.email.placeholder")}></input>
                    </div>

                    <div className="form-group">
                        <label >{t("register.username.label")}</label>
                        {nameE.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{error}</p>
                        )}
                        <input type="text" className="form-control" id="username" placeholder={t("register.username.placeholder")}></input>
                    </div>

                    <div className="form-group">
                        <label >{t("register.password.label")}</label>
                        {passE.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{error}</p>
                        )}
                        <input type="password" className="form-control" id="password" placeholder={t("register.password.placeholder")}></input>
                    </div>

                    <div className="form-group">
                        <label >{t("register.confirm.label")}</label>
                        {confirmE.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{error}</p>
                        )}
                        <input type="password" className="form-control" id="confirm" placeholder={t("register.confirm.placeholder")}></input>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>

    )

    function handleSubmit(e) {
        e.preventDefault()
        sendRequest(e.target)
    }

    async function sendRequest(form) {
        let password = form.password.value
        let username = form.username.value
        let array = []
        let mail = checkMail()
        let number = numberCheck(password)
        let length = lengthCheck(password)
        let confirm = checkConfirm(password)
        if (mail === false) {
            array.push(t("register.errors.email"))
            setEmailE([t("register.errors.email")])
        } else {
            setEmailE([])
        }

        if (length !== false && number === false) {
            array.push(t("register.errors.number"))
            setPassE([t("register.errors.number")])
        }
        if (number === false && length === false) {
            array.push(t("register.errors.lenght"))
            setPassE([t("register.errors.number"), t("register.errors.lenght")])
        }

        if (number === true && length === false) {
            array.push(t("register.errors.lenght"))
            setPassE(passE.concat([t("register.errors.lenght")]))
        }
        if (number === true && length === true) setPassE([])

        if (confirm === false) {
            array.push(t("register.errors.confirm"))
            setConfirmE([t("register.errors.confirm")])
        } else {
            setConfirmE([])
        }

        if (array.length === 0) {
            let errorsArray = []
            try {
                const response = await profil.register(username, mail, password)
                const user = await response.json()
            } catch (e) {
                setErrors([e.message])
                errorsArray.push(e)
            }


            if (errorsArray.length === 0) {
                try {
                    const response = await profil.login(mail, password)
                    const user = await response.json()
                    if (user === "bad credentials") {
                        errorsArray.push(t("login.errors.identi"))
                    }
                    setErrors(errorsArray)
                    if (errorsArray.length === 0) {
                        dispatch(changePage("home"))
                        dispatch(login(user))
                        setCookie("user", JSON.stringify(user))
                    }
                } catch (err) {
                    console.log(err)
                }
            }


        }

    }

    function checkMail() {
        let email = document.getElementById("email").value
        let isMail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        if (isMail.test(email) === false) {
            return false
        }
        return email
    }

    function numberCheck(password) {
        let hasNumber = /\d/;
        if (hasNumber.test(password) === false) {
            return false
        }
        return true
    }

    function lengthCheck(password) {
        if (password.length < 6) {
            return false
        }
        return true
    }

    function checkConfirm(password) {

        let confirm = document.getElementById("confirm").value
        if (password !== confirm) {
            return false
        }
        return true
    }
}
export default Register