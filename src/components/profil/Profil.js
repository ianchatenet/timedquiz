import React, { Fragment, useEffect } from 'react'
import { useTranslation } from "react-i18next";
import { useSelector } from 'react-redux';
import AccountInfo from './AccountInfo'
import AccountStats from './AccountStats'
import { useState } from 'react';
import UpdateProfil from './UpdateProfil'
import DeleteConf from "../confirmation/DeleteConf"
import { profil } from "../../api"
import { login } from "../../redux/actions"
import { setCookie, removeCookie } from '../../cookie/cookies';
import { useDispatch } from 'react-redux'
import { Redirect } from 'react-router-dom';
import i18n from 'i18next';


export default function Profil() {
    const dispatch = useDispatch()
    const { t } = useTranslation('profil');
    const [vue, setVue] = useState("base")
    const [errors, setErrors] = useState([])
    const [vueForm, setVueForm] = useState("base")
    const [messages, setMessages] = useState([])
    const [confirm, setConfirm] = useState(false)
    let lang = i18n.languages[0]

    const user = useSelector(state => state.user)
    useEffect(()=>{
        refreshUser()
    },[])
    return (
        <div className="text-center fadeIn">
            <div className="col-12 shadow p-3 mb-5 bg-white rounded titleBar">
                <h2>{t("title")}</h2>
            </div>
            {vue === "logout" &&
                <Redirect to="/" />
            }
            {vue === "base" &&
                <Fragment>
                    <DeleteConf context="profil" modal={confirm} deleteItem={deleteProfil} closeModal={closeConfirm}/>
                    <div className="container">
                        <div className="col-md-6 offset-md-3">
                            <ul className="p-0 list-group">
                                {messages.map((message, index) =>
                                    <li key={index} className="list-group-item list-group-item-success fadeOut">{message}</li>
                                )}
                            </ul>

                        </div>
                        <div className="row">
                            <div className="col-md-4 offset-md-1 col-10 offset-1 shadow pb-3 bg-white mb-5">
                                <h4>{user.username}</h4>
                                {lang === "fr" &&
                                    <p>{filterTitle(user.userTitles).titleFr}</p>
                                }
                                {lang === "en" &&
                                    <p>{filterTitle(user.userTitles).titleEn}</p>
                                }

                                <div className="mb-2">
                                    <button className="btn btn-primary btn-block" onClick={setUpdateVue}>{t("update")}</button>
                                </div>
                                <div>
                                    <button className="btn btn-danger btn-block" onClick={openConfirm}>{t("delete")}</button>
                                </div>
                            </div>
                            <AccountInfo user={user} />
                            <AccountStats user={user} />
                        </div>
                    </div>
                </Fragment>
            }

            {vue === "update" &&
                <UpdateProfil goBack={goBack} user={user} handleSubmit={handleSubmit} errors={errors} changeVue={changeVue} vue={vueForm} titles={user.userTitles}/>
            }
        </div>
    )

    async function refreshUser(){
        const resp = await profil.refresh(user)
        const data = await resp.json()
        dispatch(login(data))
    }
    function filterTitle(userTitles) {
        let title = userTitles.filter(function (title) {
            return title.state === true
        });
        if(title.length<1)return[""]
        return title[0].Title
    }

    function setUpdateVue() {
        setVue("update")
    }

    function goBack() {
        setVue("base")
    }

    function openConfirm(){
        setConfirm(true)
    }

    function closeConfirm(){
        setConfirm(false)
    }

    function deleteProfil() {
            profil.delete(user)
            removeCookie("user")
            setVue("logout")
            dispatch(login(false))
            setConfirm(false)
    }


    async function handleSubmit(e) {
        e.preventDefault()
        let form = e.target
        let userU = {
            "username": "",
            "email": "",
            "ancientPassword": "",
            "newPassword": "",
            "title": form.title.value
        }
        if (vueForm === "mdp") {
            userU.ancientPassword = form.ancientPass.value
            userU.newPassword = form.newPass.value
        }
        let checked = checkErrors(form)
        if (checked.length < 1) {
            if (form.username.value !== "") userU.username = form.username.value
            if (form.email.value !== "") userU.email = form.email.value

            if (checked.length > 0) return
            let resp = await profil.update(user.apiToken, userU)
            let data = await resp.json()

            if (resp.status !== 200) {
                if(data[0]==="wrong password"){
                    setErrors([t("updateForm.errors.pass")])
                }
                
            } else {
                dispatch(login(data))
                setCookie("user", JSON.stringify(data))
                setVue("base")
                setMessages([t("updateDo")])
            }

        }
    }
    function changeVue() {
        if (vueForm === "mdp") {
            setVueForm("base")
            return
        }
        setVueForm("mdp")
    }

    function checkErrors(form) {
        let array = []
        let email = form.email.value
        let isMail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        let hasNumber = /\d/;

        if (email !== "") {
            if (isMail.test(email) === false) {
                array.push(t("invalidMail"))
            }
        }

        if (vueForm === "mdp") {
            if (hasNumber.test(form.newPass.value) === false) array.push(t("mdpNbr"))

            if (form.newPass.value.length < 6) array.push(t("passLength"))
            if (form.newPass.value && form.ancientPass.value === "") array.push(t("ancientPass"))
        }

        setErrors(array)
        return array
    }
}