import React from "react"
import { useTranslation } from "react-i18next";

export default function (props) {
    const { t } = useTranslation('profil');

    let user = props.user
    return (
        <div className="col-10 offset-1 shadow bg-white rounded">
            <h5>{t("stats.title")}</h5>

            <div className="row border-bottom border-grey">
                <div className="col-md-6 text-md-right">
                    <p>{t("stats.bScore")}</p>
                </div>
                <div className="col-md-6 text-md-left text-center">
                    <p>{getBestScore(user.games)}</p>
                </div>
            </div>

            <div className="row border-bottom border-grey">
                <div className="col-md-6 text-md-right">
                    <p>{t("stats.level")}</p>
                </div>
                <div className="col-md-6 text-md-left text-center">
                    <p>{Math.floor((user.xp) / 10) + 1}</p>
                </div>
            </div>

            <div className="row border-bottom border-grey">
                <div className="col-md-6 text-md-right">
                    <p>{t("stats.ratio")}</p>
                </div>
                <div className="col-md-6 text-md-left text-center">
                    {isNaN(getAnsweredRatio(user.games)) ?
                        <p>{t("stats.nPlayed")}</p>
                        :
                        <p>{getAnsweredRatio(user.games)}%</p>
                    }

                </div>
            </div>

            <div className="row border-bottom border-grey">
                <div className="col-md-6 text-md-right">
                <p>{t("stats.nbrGames")}</p>
                </div>
                <div className="col-md-6 text-md-left text-center">
                    <p>{user.games.length}</p>
                </div>
            </div>

            <div className="row border-bottom border-grey">
                <div className="col-md-6 text-md-right">
                <p>{t("stats.nbrQuest")}</p>
                </div>
                <div className="col-md-6 text-md-left text-center">
                    <p>{getNbrQuestions(user.games)}</p>
                </div>
            </div>

        </div>
    )

    function getBestScore(array) {
        let bestScore = 0
        array.forEach(element => {
            if (element.score > bestScore) bestScore = element.score
        });
        return bestScore
    }

    function getAnsweredRatio(array) {
        let nbrGames = 0
        let ratioTotal = 0
        array.forEach(element => {
            let ratioElm = checkGamesRatio(element)
            ratioTotal += ratioElm
            nbrGames++
        });
        let ratio = ratioTotal / nbrGames
        return Math.round(ratio * 100)
    }

    function checkGamesRatio(array) {
        let correct = 0
        let nbrQuestions = 0
        array.questionsAskeds.forEach(element => {
            nbrQuestions++
            if (element.answered === true) correct++
        });
        let ratio = correct / nbrQuestions
        return ratio
    }

    function getNbrQuestions(array) {
        let nbrQuestions = 0
        array.forEach(element => {
            nbrQuestions += countQuestions(element)
        });
        return nbrQuestions
    }

    function countQuestions(array) {
        let nbrQuestions = 0
        array.questionsAskeds.forEach(element => {
            nbrQuestions++
        });
        return nbrQuestions
    }
}