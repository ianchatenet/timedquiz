import React, { Fragment } from "react"
import { useTranslation } from "react-i18next";
import i18n from 'i18next';




export default function UpdateProfil(props) {
    const { t } = useTranslation('profil');
    let handleSubmit = props.handleSubmit
    let errors = props.errors
    let changeVue = props.changeVue
    let vue = props.vue
    let goBack = props.goBack
    let titles = sortTitleArray(props.titles)
    let lang = i18n.languages[0]

    return (
        <Fragment>
            <button className="btn btn-primary mb-1" onClick={goBack}>{t("goBack")}</button>
            <form onSubmit={handleSubmit} className="col-10 offset-1 shadow pb-3 bg-white mb-5">
                <div className="col-md-6 offset-md-3">
                    <ul className="p-0 list-group">
                        {errors.map((error, index) =>
                            <li key={index} className="list-group-item list-group-item-danger">{error}</li>
                        )}
                    </ul>

                </div>

                <div className="form-group">
                    <label >{t("updateForm.username.label")}</label>
                    <input type="text" className="form-control" id="username" placeholder={t("updateForm.username.placeHolder")}></input>
                </div>

                <div className="form-group">
                    <label >{t("updateForm.title.label")}</label>
                    <select className="form-control" id="title">
                                {
                                    titles.map((title, index) =>
                                        <option key={index} value={title.id}>
                                            {lang === "fr" &&
                                                 title.Title.titleFr 
                                            }
                                            {lang === "en" &&
                                                 title.Title.titleEn 
                                            }
                                        </option>
                                    )
                                }
                    </select>
                </div>

                <div className="form-group">
                    <label >{t("updateForm.email.label")}</label>
                    <input type="text" className="form-control" id="email" placeholder={t("updateForm.email.placeHolder")}></input>
                </div>


                <div>
                    <a className="btn btn-primary text-white" onClick={changeVue}>{t("passwordChange")}</a>
                </div>

                {vue === "mdp" &&
                    <div className="form-group">
                        <label>{t("updateForm.password.label")}</label>
                        <input type="text" className="form-control" id="ancientPass" placeholder={t("updateForm.password.placeHolderAnc")}></input>
                        <input type="text" className="form-control" id="newPass" placeholder={t("updateForm.password.placeHolderNew")}></input>
                    </div>
                }

                <button className="btn btn-primary mt-2">{t("updateForm.update")}</button>
            </form>
        </Fragment>

    )
    function sortTitleArray(userTitles) {
        userTitles.sort(function (titles1, titles2) {
            return titles2.state - titles1.state
        })
        return userTitles
    }
}