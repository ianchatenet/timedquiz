import React from "react";
import { useTranslation } from "react-i18next";

export default function AccountInfo(props) {
    const { t } = useTranslation('profil');
    let user = props.user
    let registerDate = new Date(user.createdAt)

    return (
        <div className="col-md-5 offset-md-1 col-10 offset-1 mb-5 shadow bg-white rounded">

            <div className="row border-bottom border-grey">
                <div className="col-md-4 text-md-right">
                    <p>{t("info.mail")}</p>
                </div>
                <div className="col-md-8 text-md-left text-center">
                    <p>{user.email}</p>
                </div>
            </div>

            <div className="row border-bottom border-grey">
                <div className="col-md-4 text-md-right">
                    <p>{t("info.role")}</p>
                </div>
                <div className="col-md-8 text-md-left text-center">
                    <p>{user.roles[0].slice(5).toLowerCase()}</p>
                </div>
            </div>

            <div className="row border-bottom border-grey">
                <div className="col-md-4 text-md-right">
                    <p>{t("info.register")}</p>
                </div>
                <div className="col-md-8 text-md-left text-center">
                    <p>{registerDate.getDate() + "/" + registerDate.getMonth() + "/" + registerDate.getFullYear()}</p>
                </div>
            </div>
        </div>
    )
}