import React from 'react'
import Modal from 'react-modal';
import { useTranslation } from "react-i18next";


export default function DeleteConf(props) {

    const { t } = useTranslation('confirm');

    let modal = props.modal
    let context = props.context
    let deleteItem = props.deleteItem
    let closeModal = props.closeModal
    const secondStyles = {
        content: {
            top: '20%',
            left: '30%',
            right: '30%',
            width: "40%",
            height: "40%"
        }
    };
    let content = checkContext(context)

    return (
        <Modal isOpen={modal} style={secondStyles}>
            <div className="container text-center">
                <p>{content}</p>
                <div className="row justify-content-around">
                    <button onClick={closeModal} className="btn btn-primary">{t("cancel")}</button>
                    <button onClick={deleteItem} className='btn btn-primary'>{t('delete')}</button>
                </div>
            </div>

        </Modal>
    )

    function checkContext(context) {
        if (context === "question") return t("contentQuest")
        if (context === "theme") return t('contentTheme')
        if(context ==="profil") return t('contentProfil')
    }
}
