import React, { useState, useEffect } from "react";
import Loading from "../loader/Loading"
import { modo, mytholo } from "../../api"
import { useTranslation } from "react-i18next";


export default function AddQuestion(props) {
    const { t } = useTranslation('modo');

    const [optionNbr, setOptionNbr] = useState("1")
    const [errors, setErrors] = useState([])
    const [loading, setLoading] = useState(false)
    const [themes, setThemes] = useState()
    const [needOptE, setNeedOptE] = useState([])
    const [langQuesE, setLangQuesE] = useState([])
    const [langOptE, setLangOptE] = useState([])
    const [checkTrue, setCheckTrue] = useState([])
    let user = props.user
    let goBack = props.goBack


    useEffect(() => {
        loadThemes();
    }, [])

    return (
        <div className="shadow p-3 mb-5 bg-white rounded col-10 offset-1">
            <button className="btn btn-primary" onClick={goBack}>{t("add.goBack")}</button>
            {loading == true ?
                <form onSubmit={handleSubmit}>
                    <h5>{t("add.suggest")}</h5>

                    <div className="form-group">
                        <label >{t("add.question.label")}</label>
                        {langQuesE.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{error}</p>
                        )}
                        <input type="text" className="form-control" id="questionFr" placeholder={t("add.question.placeHolderFr")}></input>
                        <input type="text" className="form-control" id="questionEn" placeholder={t("add.question.placeHolderEn")}></input>
                    </div>

                    <div className="form-group">
                        <label>{t("add.nbrOptions.label")}</label>
                        <select className="form-control" id="optionNbr" onChange={handleOptions}>
                            <option value={1}>1</option>
                            <option value={2}>2</option>
                            <option value={3}>3</option>
                            <option value={4}>4</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label>{t("add.options.label")}</label>
                        {needOptE.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{error}</p>
                        )}
                        {langOptE.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{error}</p>
                        )}
                        {checkTrue.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{error}</p>
                        )}
                        {optionNbr > 0 &&

                            <div className="form-group">
                                <input type="text" className="form-control" id="Option1Fr" placeholder={t("add.options.option1Fr")}></input>
                                <input type="text" className="form-control" id="Option1En" placeholder={t("add.options.option1En")}></input>
                                <select className="form-control" id="Option1Cor" >
                                    <option value={false}>{t("add.options.false")}</option>
                                    <option value={true}>{t("add.options.true")}</option>
                                </select>
                            </div>
                        }
                        {optionNbr > 1 &&

                            <div className="form-group">
                                <input type="text" className="form-control" id="Option2Fr" placeholder={t("add.options.option2Fr")}></input>
                                <input type="text" className="form-control" id="Option2En" placeholder={t("add.options.option2En")}></input>
                                <select className="form-control" id="Option2Cor">
                                    <option value={false}>{t("add.options.false")}</option>
                                    <option value={true}>{t("add.options.true")}</option>
                                </select>
                            </div>
                        }
                        {optionNbr > 2 &&
                            <div className="form-group">
                                <input type="text" className="form-control" id="Option3Fr" placeholder={t("add.options.option3Fr")}></input>
                                <input type="text" className="form-control" id="Option3En" placeholder={t("add.options.option3En")}></input>
                                <select className="form-control" id="Option3Cor" >
                                    <option value={false}>{t("add.options.false")}</option>
                                    <option value={true}>{t("add.options.true")}</option>
                                </select>
                            </div>
                        }
                        {optionNbr > 3 &&
                            <div className="form-group">
                                <input type="text" className="form-control" id="Option4Fr" placeholder={t("add.options.option4Fr")}></input>
                                <input type="text" className="form-control" id="Option4En" placeholder={t("add.options.option4En")}></input>
                                <select className="form-control" id="Option4Cor" >
                                    <option value={false}>{t("add.options.false")}</option>
                                    <option value={true}>{t("add.options.true")}</option>
                                </select>
                            </div>
                        }
                    </div>

                    {themes &&
                        <div className="form-group">
                            <label>{t("add.themes.label")}</label>
                            <select className="form-control" id="theme">
                                {themes.map((theme, index) =>
                                    <option key={index} value={theme.id}>{theme.themeFr} {theme.themeEn}</option>
                                )}
                            </select>
                        </div>
                    }

                    <div className="form-group">
                        <label>{t("add.dif.label")}</label>
                        <select className="form-control" id="dif">
                            <option value={1}>1</option>
                            <option value={2}>2</option>
                            <option value={3}>3</option>
                        </select>
                    </div>


                    <button type="submit" className="btn btn-primary">{t("add.submit")}</button>
                </form>
                :
                <Loading />
            }
        </div >
    )
    async function loadThemes() {
        const response = await mytholo.getThemes()
        const data = await response.json()
        setLoading(true)
        setThemes(data)
    }


    function handleOptions(e) {
        setOptionNbr(parseInt(e.target.value))
    }
    function handleSubmit(e) {
        e.preventDefault()

        let array = checkForm(e)
        setErrors(array)
        let options = addOption(e)

        let question = {
            "questionFr": e.target.questionFr.value,
            "questionEn": e.target.questionEn.value,
            "options": options,
            "difficulty": e.target.dif.value,
            "state": false,
            "theme": e.target.theme.value
        }
        if (array.length < 1) {
            modo.createQuestion(user.apiToken, question)
            goBack()
        }
    }

    function addOption(e) {
        let options = []
        if (optionNbr > 0) {
            let option = {
                "optionFr": e.target.Option1Fr.value,
                "optionEn": e.target.Option1En.value,
                "isCorrect": e.target.Option1Cor.value
            }
            options.push(option)
        }
        if (optionNbr > 1) {
            let option = {
                "optionFr": e.target.Option2Fr.value,
                "optionEn": e.target.Option2En.value,
                "isCorrect": e.target.Option2Cor.value
            }
            options.push(option)
        }
        if (optionNbr > 2) {
            let option = {
                "optionFr": e.target.Option3Fr.value,
                "optionEn": e.target.Option3En.value,
                "isCorrect": e.target.Option3Cor.value
            }
            options.push(option)
        }
        if (optionNbr > 3) {
            let option = {
                "optionFr": e.target.Option4Fr.value,
                "optionEn": e.target.Option4En.value,
                "isCorrect": e.target.Option4Cor.value
            }
            options.push(option)
        }
        return options
    }

    function checkForm(e) {
        let form = e.target
        let array = []

        if (checkCor(e) === false) {
            setNeedOptE([t('add.errors.needOpt')])
            array.push(t('add.errors.needOpt'))
        }

        if (form.questionFr.value === "" || form.questionEn.value === "") {
            setLangQuesE([t("add.errors.langQues")])
            array.push(t("add.errors.langQues"))
        }

        if (form.Option1Fr.value === "" || form.Option1En.value === "") {
            setLangOptE([t('add.errors.langOpt')])
            array.push(t("add.errors.langOpt"))
        }

        if (optionNbr > 1) {
            if (form.Option2Fr.value ==="" || form.Option2En.value ==="") array.push(t("add.errors.langOpt")+ " option 2")
            if (form.Option2Cor.value === "true" && (form.Option2Cor.value === form.Option1Cor.value)) {
                setCheckTrue([t("add.errors.checkTrue")])
                array.push(t("add.errors.checkTrue"))
            }
        }

        if (optionNbr > 2) {
            if (form.Option3Fr.value ==="" || form.Option3En.value==="") array.push(t("add.errors.langOpt")+ " option 3")
            if (form.Option3Cor.value === "true" && (form.Option3Cor.value === form.Option1Cor.value || form.Option3Cor.value === form.Option2Cor.value || form.Option3Cor.value === form.Option4Cor.value)) {
                setCheckTrue([t("add.errors.checkTrue")])
                array.push(t("add.errors.checkTrue"))
            }

        }
        if (optionNbr > 3) {
            if (form.Option4Fr.value==="" || form.Option4En.value==="") array.push(t("add.errors.langOpt")+ " option 4")
            if (form.Option4Cor.value === "true" && (form.Option4Cor.value === form.Option1Cor.value || form.Option4Cor.value === form.Option2Cor.value || form.Option4Cor.value === form.Option3Cor.value)) {
                setCheckTrue([t("add.errors.checkTrue")])
                array.push(t("add.errors.checkTrue"))
            }
        }
        return  [...new Set(array)];
    }

    function checkCor(e) {
        let array = [e.target.Option1Cor.value]
        if (optionNbr > 1) array.push(e.target.Option2Cor.value)
        if (optionNbr > 2) array.push(e.target.Option3Cor.value)
        if (optionNbr > 3) array.push(e.target.Option4Cor.value)
        return array.includes("true")
    }
}