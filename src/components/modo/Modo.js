import React, { useState } from 'react'
import AddQuestion from './AddQuestion'
import { useSelector } from 'react-redux'
import { useTranslation } from "react-i18next";

export default function Modo() {
    const { t } = useTranslation('modo');

    // const { t } = useTranslation('admin');
    let user = useSelector(state => state.user)
    const [vue, setVue] = useState("")

    return (
        <div className="text-center" id="container">
            <div className="col-12 shadow p-3 mb-5 bg-white rounded titleBar fadeIn">
                <h3>{t("title")}</h3>
            </div>

    <button className="btn btn-primary btn-lg m-3" onClick={handleClick} id="questions">{t('suggest')}</button>
            {/* <button className="btn btn-primary btn-lg m-3" onClick={handleClick} id="users">{t("users")}</button>
            <button className="btn btn-primary btn-lg m-3" onClick={handleClick} id="themes">{t("themes")}</button> */}

            <div>
                {vue === "questions" &&
                    <AddQuestion user={user} goBack={goBack} />
                }
            </div>
        </div>
    )

    function handleClick(e) {
        let id = e.target.id
        setVue(id)
    }

    function goBack() {
        setVue("base")
    }
}