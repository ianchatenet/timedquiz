import React from 'react'
import Stars from "./Stars.js"
function QuestionContent(props) {
    let questions = props.questions
    let turn = props.turn
    let timeLeft = props.timeLeft
    let lang = props.lang
    return (
        <div>
            <div className="col-12">
                {lang === 'fr' ?
                    <p className="question">{questions[turn].questionFr}</p>
                    :
                    <p className="question">{questions[turn].questionEn}</p>
                }
                
            </div>
            <div className="col-12">
                <Stars dif={questions[turn].difficulty} />
            </div>

            <div className="col-12">
                <p className="timer">{timeLeft}</p>
            </div>
        </div>)
}
export default QuestionContent