import React from 'react'
import { useTranslation } from "react-i18next";

function Infobar(props) {

    const { t } = useTranslation('quiz');//translation

    let questions = props.questions
    let turn = props.turn
    let lang = props.lang

    return (
        <div className="col-12 shadow p-3 mb-5 bg-white rounded titleBar">
            <div className="row">
                <div className="col">
                    {lang === "fr" &&
                        <h4>{t('infobar.myth')} {questions[turn].theme.themeFr}</h4>
                    }

                    {lang === "en" &&
                        <h4>{questions[turn].theme.themeEn} {t('infobar.myth')}</h4>
                    }

                </div>
                <div className="col">
                    <h5>{turn + 1}/{questions.length}</h5>
                </div>
            </div>
        </div>
    )
}
export default Infobar