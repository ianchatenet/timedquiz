import React, { useState, useEffect } from 'react';
import QuestionContent from './QuestionContent.js';
import Infobar from './Infobar';
import Endquiz from './Endquiz';
import Infoquiz from './Infoquiz';
import Lifes from './Lifes';
import { useTranslation } from "react-i18next";
import { useSelector } from 'react-redux';
import { quiz } from "../../../api";
import i18n from 'i18next';

function Questions(props) {
    const { t } = useTranslation('quiz');//translation

    let questions = props.questions

    let initString = {
        content: "",
        reset: false
    }
    let lang = i18n.languages[0]

    //state
    const [timeLeft, setTimeLeft] = useState(props.time);
    const [turn, setTurn] = useState(0);
    const [answer, setAnswer] = useState(initString)
    const [inputValue, setInputValue] = useState()
    const [answered, setAnswered] = useState()
    const [lifes, setLifes] = useState(0)
    const [buttonDisabled, setButtonDisabled] = useState(false)

    const [score, setScore] = useState(0)
    const [questionsAnswered, setQuestionsAnswered] = useState([])


    let user = useSelector(state => state.user)
    useEffect(() => {
        return timer()
    });

    //change nbr lifes when turn change
    useEffect(() => {
        setAnswered(false)
        initLifes()
        if (user != false && turn >= questions.length) {
            quiz.createGame(user, questionsAnswered, score)
        }
    }, [turn])


    return (
        <div className="justify-content-center text-center">
            {turn >= questions.length ?
                <Endquiz questions={questions} score={score} turn={turn} lang={lang} />
                :
                <div>
                    <Infobar questions={questions} turn={turn} lang={lang}/>

                    <div className="shadow p-3 mb-5 bg-white rounded col-8 offset-2">

                        <QuestionContent questions={questions} turn={turn} timeLeft={timeLeft} lang={lang} />
                        <Infoquiz answer={answer} />
                        {questions[turn].options.length > 1 ?
                            <div className="row justify-content-center">
                                {questions[turn].options.map((option, index) =>
                                    <div className="col-md-12 text-center" key={index}>
                                        {lang === "fr" ?
                                            <button className="btn btn-primary selectBtn btn-lg mb-2" disabled={buttonDisabled} onClick={handleClick} value={option.optionFr}>{option.optionFr}</button>
                                            :
                                            <button className="btn btn-primary selectBtn btn-lg mb-2" disabled={buttonDisabled} onClick={handleClick} value={option.optionEn}>{option.optionEn}</button>
                                        }
                                    </div>
                                )}
                            </div>
                            :
                            <form onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <div className="col-lg-6 offset-lg-3">
                                        <input className="form-control input" type="text" id="input" onChange={handleChange} placeholder={t('question.input.label')} autoComplete="off" />
                                    </div>
                                    <Lifes lifes={lifes} />
                                </div>
                                <input className="btn btn-primary" type="submit" value={t('question.submit.value')} disabled={buttonDisabled} />
                            </form>
                        }
                    </div>
                </div>
            }
        </div>
    );
    // quiz system

    function initLifes() {
        if (turn < questions.length) {
            setLifes(questions[turn].difficulty + 1)
        }
    }

    function timer() {
        let interval
        if (turn < questions.length) {
            interval = setInterval(() => {
                if (timeLeft <= 0) {
                    setTurn(turn => turn + 1)
                    setTimeLeft(props.time)
                    setButtonDisabled(false)
                    if (answered === false) getQuestionsAsked(false)
                } else {
                    setTimeLeft(timeLeft => timeLeft - 1);
                }
            }, 1000);
        }
        return () => clearInterval(interval);
    }


    function handleChange(e) {
        setInputValue(e.target.value)
    }

    function handleSubmit(e) {
        e.preventDefault()
        let result = checkAnswer(inputValue)
        setLifes(lifes - 1)
        setAnswered(true)
        if (lifes === 1 || result === true) {
            setButtonDisabled(true)
            setTimeLeft(0)
            getQuestionsAsked(result)
        }
        document.getElementById("input").value = ""
    }


    function handleClick(e) {
        let value = e.target.value
        setButtonDisabled(true)
        let result = checkAnswer(value)
        getQuestionsAsked(result)
        setAnswered(true)
        if (result === false) {
            setTimeLeft(0)
        }
    }

    function checkAnswer(value) {
        const isCorrect = questions[turn].options.find(option => option.isCorrect === true);

        if (value === isCorrect.optionFr || value === isCorrect.optionEn) {

            setTimeLeft(0)
            setScore(score + (props.time - (props.time - timeLeft)) * 10)

            setAnswer({
                content: t("message.true"),
                reset: !answer.reset
            })
            return true
        }
        setAnswer({
            content: t("message.false"),
            reset: !answer.reset
        })
        return false
    }

    function getQuestionsAsked(answ) {
        let resp = [[questions[turn].id, answ]]
        let array = questionsAnswered.concat(resp)
        setQuestionsAnswered(array)
    }
}

export default Questions;