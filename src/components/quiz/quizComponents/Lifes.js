import React, { Fragment } from 'react'

function Lifes(props) {
    let lifes = props.lifes
    let test = []
    for (let i = 0; i < lifes; i++) {
        test.push(i)
    }
    return (
        <Fragment>
            {
                test.map((index) =>
                    <svg width="1em" key={index} height="1em" viewBox="0 0 16 16" className=" m-1 bi bi-heart-fill hearth" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
                    </svg>)
            }
        </Fragment>
    )
}

export default Lifes