import React from 'react'
import { useTranslation } from "react-i18next";



function Infoquiz(props) {
    const { t } = useTranslation('quiz');//translation

    let answer = props.answer

    return (
        <div className="mt-3">
            
            {answer.reset === false && answer.content === t("message.false") &&
                <p className="bg-danger text-white  blink">{answer.content}</p>
            }

            {answer.reset === true && answer.content === t("message.false") &&
                <p className="bg-danger text-white  blink">{answer.content}</p>
            }

            {answer.reset === false && answer.content === t("message.true") &&
                <p className="bg-success text-white  blink">{answer.content}</p>
            }

            {answer.reset === true && answer.content === t("message.true") &&
                <p className="bg-success text-white  blink">{answer.content}</p>
            }


        </div>
    )
}
export default Infoquiz