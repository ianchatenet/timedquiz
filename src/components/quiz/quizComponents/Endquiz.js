import React, { Fragment } from "react"
import { useTranslation } from "react-i18next";


function Endquiz(props) {
    const { t } = useTranslation('quiz');

    let questions = props.questions
    let score = props.score
    let turn = props.turn
    let lang= props.lang
    
    return (
        <div>
            <h2 className="col-12 shadow p-3 mb-5 bg-white rounded titleBar">
                {t('endquiz.title')}
            </h2>
            <p>{turn} {t('endquiz.question')}</p>
            <p>{t('endquiz.score')}: {score}</p>
            {questions.map((question, index) =>
                <div className="shadow p-3 mb-5 bg-white rounded col-10 col-sm-6 offset-1 offset-sm-3" key={index}>
                    {lang === "fr"?
                    <Fragment>
                        <h5>{question.questionFr}</h5>
                        <p>{t('endquiz.response')} {question.options.find(option => option.isCorrect === true).optionFr}</p>
                        <p>{question.desc}</p>
                    </Fragment>  
                :
                <Fragment>
                    <h5>{question.questionEn}</h5>
                    <p>{t('endquiz.response')} {question.options.find(option => option.isCorrect === true).optionEn}</p>
                    <p>{question.desc}</p>
                </Fragment>
                }    
                </div>
            )}
        </div>
    )
}
export default Endquiz