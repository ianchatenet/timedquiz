import React, { useState } from 'react';
import Question from './quizComponents/Questions.js';
import './quiz.css';
import { useTranslation } from "react-i18next";
import Loading from '../loader/Loading.js';
import {quiz} from '../../api'
// language change

function Quiz() {
    const { t } = useTranslation('quiz');

    const [loading, setLoading] = useState(false)
    const [game, setGame] = useState(false)
    const [questions, setQuestions] = useState()

    let time = 15
    return (
        <div className="fadeIn">

            {game === false ?
                <div className="text-center">
                    <div className=" col-12 shadow p-3 mb-5 bg-white rounded titleBar">
                        <h3>{t("quiz.title")}</h3>
                    </div>

                    <button className="btn btn-primary btn-lg" onClick={handleClick}>{t('quiz.start')}</button>
                </div>
                : loading === false ?
                    <Loading />

                    :
                    <Question time={time} questions={questions} />
            }
        </div>
    );

     async function handleClick() {
        setGame(true)

            const response = await quiz.getQuiz()
            const data = await  response.json()
            setQuestions(data)
            setLoading(true)
    }
}

export default Quiz;