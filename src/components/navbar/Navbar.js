import React, { Fragment, useState } from 'react';
import {
    Route,
    Link,
    Redirect,
    Switch,
    useLocation
} from "react-router-dom";
import i18n from '../../helpers/i18n.js';
import Home from '../home/Home.js';
import Quiz from '../quiz/Quiz.js';
import Profil from '../profil/Profil.js';
import Admin from "../admin/Admin.js";
import Login from '../authentication/Login.js';
import Register from '../authentication/Register.js';
import NoMatch from '../../errorPages/NoMatch.js';
import NotAllowed from '../../errorPages/NotAllowed';
import Modo from '../modo/Modo';
import 'bootstrap/js/dist/dropdown';
import { useTranslation } from "react-i18next";
import LangButton from './LangButton';
import './navbar.css'
import { useSelector, useDispatch } from 'react-redux'
import { removeCookie } from '../../cookie/cookies.js';
import { profil } from "../../api";
import { login, changePage } from "../../redux/actions"
import AlreadyLogged from '../../errorPages/AlreadyLogged.js';
import logo from './logo.png'

function Navbar() {
    const dispatch = useDispatch()
    const user = useSelector(state => state.user)
    const { t } = useTranslation('app');
    const location = useLocation()
    const currentPage = useSelector(state => state.page)
    const [collapse, setCollapse] = useState("collapse")

    navChange()//change link's style in nav

    return (

        <div>
            <nav className="navbar navbar-expand-lg sticky-top navbar-dark bg-blue p-0 text-center titleBar">
                <div className="pl-2">
                    <Link to="/" className="navbar-brand"><img src={logo} className="brandImg" alt="Logo de mytholoquiz" /></Link>
                    <button className="navbar-toggler" onClick={handleCollapse} >
                        <span className="navbar-toggler-icon"></span>
                    </button>
                </div>

                    <div className="d-lg-none">
                        {user && <Link to="/profil" className="mr-4 text-white">{user.username}</Link>}
                        <LangButton langSwitch={languageSwitch} />
                    </div>
                

                <div className={collapse + " navbar-collapse"}>
                    <ul className="navbar-nav mr-auto">
                        <li className="navLink">
                            {currentPage === "/home" ?
                                <Link to="/" className="nav-link text-white actualPage pl-1 pr-1" onClick={handleCollapse} >{t('navbar.home')}</Link>
                                :
                                <Link to="/" className="nav-link text-white pl-1 pr-1" onClick={handleCollapse}>{t('navbar.home')}</Link>
                            }
                        </li>
                        <li className="navLink">
                            {currentPage === "/quiz" ?
                                <Link to="/quiz" className="nav-link text-white actualPage pl-1 pr-1" onClick={handleCollapse} >{t('navbar.quiz')}</Link>
                                :
                                <Link to="/quiz" className="nav-link text-white pl-1 pr-1" onClick={handleCollapse}>{t('navbar.quiz')}</Link>
                            }
                        </li>
                        {user &&
                            <Fragment>
                                <li className="navLink">
                                    {currentPage === "/profil" ?
                                        <Link to="/profil" className="nav-link text-white actualPage pl-1 pr-1" onClick={handleCollapse}>{t('navbar.profil')}</Link>
                                        :
                                        <Link to="/profil" className="nav-link text-white pl-1 pr-1" onClick={handleCollapse}>{t('navbar.profil')}</Link>
                                    }
                                </li>

                                {(user.roles.includes("ROLE_ADMIN") || user.roles.includes("ROLE_MODO")) &&
                                    <li className="navLink">
                                        {currentPage === "/modo" ?
                                            <Link to="/modo" className="nav-link text-white actualPage pl-1 pr-1" onClick={handleCollapse}>{t('navbar.modo')}</Link>
                                            :
                                            <Link to="/modo" className="nav-link text-white pl-1 pr-1" onClick={handleCollapse}>{t('navbar.modo')}</Link>
                                        }
                                    </li>
                                }

                                {user.roles.includes("ROLE_ADMIN") &&
                                    <li className="navLink">
                                        {currentPage === "/admin" ?
                                            <Link to="/admin" className="nav-link text-white actualPage pl-1 pr-1" onClick={handleCollapse}>{t('navbar.admin')}</Link>
                                            :
                                            <Link to="/admin" className="nav-link text-white pl-1 pr-1" onClick={handleCollapse}>{t('navbar.admin')}</Link>
                                        }
                                    </li>
                                }

                            </Fragment>
                        }

                    </ul>
                    {user ?
                        <Fragment>

                            <Link to="/" className="btn btn-danger text-white mr-4 navbar-btn" onClick={handleLogOut}>{t('navbar.logout')}</Link>

                        </Fragment>
                        :
                        <Fragment>
                            <Link to="/login" className="text-white btn btn-success navbar-btn mr-4" onClick={handleCollapse}>{t('navbar.login')}</Link>

                            <Link to="/register" className="text-white btn btn-success navbar-btn mr-4" onClick={handleCollapse}>{t('navbar.register')}</Link>
                        </Fragment>
                    }
                </div>


                <div className="d-lg-inline d-none">
                    {user && <Link to="/profil" className="mr-4 text-white">{user.username}</Link>}
                    <LangButton langSwitch={languageSwitch} />
                </div>


            </nav>
            <Switch>
                <Route exact path="/home">
                    <Home />
                </Route>
                <Route exact path="/quiz">
                    <Quiz />
                </Route>
                <Route exact path="/profil">
                    {user ?
                        <Profil />
                        :
                        <Redirect to="/home" />
                    }
                </Route>
                <Route exact path="/modo" >
                    {user && (user.roles.includes("ROLE_ADMIN") || user.roles.includes("ROLE_MODO")) ?
                        <Modo />
                        :
                        <NotAllowed />
                    }
                </Route>
                <Route exact path="/admin">

                    {user && user.roles.includes("ROLE_ADMIN") ?
                        <Admin />
                        :
                        <NotAllowed />
                    }
                </Route>
                <Route exact path="/login">
                    {user ?
                        <AlreadyLogged />
                        :
                        <Login />
                    }
                </Route>
                <Route exact path="/register">
                    {user ?
                        <AlreadyLogged />
                        :
                        <Register />
                    }
                </Route>
                <Route exact path="/">
                    <Redirect to="/home" />
                </Route>
                <Route>
                    <NoMatch />
                </Route>
            </Switch>
        </div>

    )

    function handleCollapse(e) {
        if (collapse === "collapse show") setCollapse("collapse")
        if (collapse === "collapse") setCollapse("collapse show")
    }

    function handleLogOut() {
        handleCollapse()
        profil.logout(user.email)
        removeCookie("user")
        dispatch(changePage("home"))
        dispatch(login(false))
    }

    function navChange() {
        if (location.pathname === "/") {
            dispatch(changePage("home"))
        } else {
            dispatch(changePage(location.pathname))
        }
    }
    // props to LangButton component
    function languageSwitch(e) {
        let lang = e.target.value
        i18n.changeLanguage(lang, (err, t) => {
            if (err) return console.log('something went wrong loading', err);
            t('key');
        });
    }
}
export default Navbar