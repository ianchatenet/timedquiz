import React from "react"
import i18n from '../../helpers/i18n.js';

function LangButton(props) {
    let lang = i18n.languages
    return (
        <div className="dropdown mr-5 navbar-brand">
            <button className="btn btn-sm btn-primary border border-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {lang[0]}
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <button className="dropdown-item" value={lang[1]} onClick={props.langSwitch}>{lang[1]}</button>
            </div>
        </div>
    )



}
export default LangButton