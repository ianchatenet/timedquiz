import React from 'react'
import { useTranslation } from "react-i18next";
import { Link } from 'react-router-dom';


export default function Home() {
    const { t } = useTranslation('home');
    return (
        <div className="text-center fadeIn">
            <div className="col-12 shadow p-3 mb-5 bg-white rounded titleBar">
                <h3>{t("title")}</h3>
            </div>
            <div className="col-10 offset-1 shadow p-3 mb-5 bg-white rounded text-center">
                <div className="row text-center justify-content-around">
                    <Link to="/login" className="btn btn-success">{t("login")}</Link>
                    <Link to="/register" className="btn btn-success">{t("register")}</Link>
                </div>

                {/* <p>{t("content.desc")}</p> */}
            </div>
        </div>
    )
}