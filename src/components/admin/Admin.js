import React, { useState } from 'react'
import { useTranslation } from "react-i18next";
import QuestionsList from './question/QuestionList.js';
import UsersList from "./user/UsersList.js";
import ThemesList from "./theme/ThemesList.js";


export default function Admin() {
    const { t } = useTranslation('admin');

    const [vue, setVue] = useState("")

    return (
        <div className="text-center" id="container">
            <div className="col-12 shadow p-3 mb-5 bg-white rounded titleBar fadeIn">
                <h3>{t("title")}</h3>
            </div>

            <button className="btn btn-primary btn-lg m-3" onClick={handleClick} id="questions">{t("questions")}</button>
            <button className="btn btn-primary btn-lg m-3" onClick={handleClick} id="users">{t("users")}</button>
            <button className="btn btn-primary btn-lg m-3" onClick={handleClick} id="themes">{t("themes")}</button>

            <div>
                {vue === "questions" &&
                    <QuestionsList />
                }
                {vue === "users" &&
                    <UsersList />
                }
                {vue === "themes" &&
                    <ThemesList />
                }
            </div>
        </div>
    )

    function handleClick(e) {
        let id = e.target.id
        setVue(id)
    }
}