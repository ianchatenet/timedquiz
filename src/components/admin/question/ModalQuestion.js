import React, { Fragment, useState } from "react";
import { useTranslation } from "react-i18next";
import Modal from 'react-modal';
import { admin } from "../../../api";
import { useSelector } from 'react-redux'
import FormUpdate from './FormUpdate'
import DeleteConf from '../../confirmation/DeleteConf'

export default function ModalQuestion(props) {
    let question = props.question
    let modal = props.modal
    let closeModal = props.closeModal
    const { t } = useTranslation('admin');

    const user = useSelector(state => state.user)

    const [vue, setVue] = useState("base")
    const [confirm, setConfirm] = useState(false)

    const customStyles = {
        content: {
            top: '10%',
            left: '20%',
            right: '20%',
            width: "60%",
        }
    }
    Modal.setAppElement("#container")

    return (
        <Modal isOpen={modal} onRequestClose={closeModal} style={customStyles}>
            <div className="text-center">

                {vue === "base" ?
                    <Fragment>
                        <h4>Question {question.id} {t('question.modal.by')}  {question.user.username}</h4>
                        <div className="row">
                            <DeleteConf modal={confirm} context="question" deleteItem={deleteQuestion} closeModal={closeConfirm}/>
                            <div className="col">
                                <p>{question.questionFr}</p>
                                {question.options.map((option, index) =>
                                    <div key={index}>
                                        <p>{option.optionFr} {option.isCorrect.toString()}</p>
                                    </div>
                                )}
                            </div>

                            <div className="col">

                                <p>{question.questionEn}</p>
                                {question.options.map((option, index) =>
                                    <div key={index}>
                                        <p>{option.optionEn} {option.isCorrect.toString()}</p>
                                    </div>
                                )}
                            </div>
                        </div>
                    </Fragment>
                    :
                    <FormUpdate handleSubmit={handleUpdate} question={question} />
                }

                {vue === "base" &&
                    <Fragment>
                        <button onClick={updateQuestion} className="btn btn-success"> {t('question.modal.update')}</button>
                        <button onClick={handleDeleteButton} className="btn btn-danger"> {t('question.modal.delete')}</button>
                    </Fragment>
                }
                {vue === "update" && <button onClick={goBack} className="btn btn-success"> {t('question.modal.return')}</button>}
                <button onClick={handleClose} className="btn btn-primary">{t('question.modal.close')}</button>
            </div>
        </Modal>
    )
    function handleClose() {
        closeModal()
        setVue("base")
    }

    function updateQuestion() {
        setVue("update")
    }
    function goBack() {
        setVue("base")
    }

    function closeConfirm(){
        setConfirm(false)
    }
    function handleDeleteButton(){
        setConfirm(true)
    }
    function deleteQuestion() {
            admin.deleteQuestion(user.apiToken, question.id)
            closeModal()
            setConfirm(false)
    }

    function handleUpdate(e) {
        e.preventDefault()
        let newQuestion = question
        if (e.target.questionFr.value) {
            newQuestion.questionFr = e.target.questionFr.value
        }
        if (e.target.questionEn.value) {
            newQuestion.questionEn = e.target.questionEn.value
        }
        if (e.target.Option1Fr.value) {
            newQuestion.options[0].optionFr = e.target.Option1Fr.value
        }
        if (e.target.Option1En.value) {
            newQuestion.options[0].optionEn = e.target.Option1En.value
        }
        if (question.options.length > 1) {
            if (e.target.Option2Fr.value) {
                newQuestion.options[1].optionFr = e.target.Option2Fr.value
            }
            if (e.target.Option2En.value) {
                newQuestion.options[1].optionEn = e.target.Option2En.value
            }
            if (e.target.Option3Fr.value) {
                newQuestion.options[2].optionFr = e.target.Option3Fr.value
            }
            if (e.target.Option3En.value) {
                newQuestion.options[2].optionEn = e.target.Option3En.value
            }
            if (e.target.Option4En.value) {
                newQuestion.options[3].optionEn = e.target.Option4En.value
            }

            if (e.target.Option4Fr.value) {
                newQuestion.options[3].optionFr = e.target.Option4Fr.value
            }
        }
        newQuestion.state = JSON.parse(e.target.state.value)
        newQuestion.difficulty = e.target.dif.value
        admin.updateQuestion(user.apiToken, newQuestion)
        setVue("base")
    }
}
