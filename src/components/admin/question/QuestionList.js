import React, { useState, useEffect, Fragment } from "react"
import Loading from '../../loader/Loading.js';
import { admin } from "../../../api"
import { useSelector } from 'react-redux'
import DataTable from 'react-data-table-component';
import ModalQuestion from './ModalQuestion';
import FormAdd from './FormAdd';
import { useTranslation } from "react-i18next";


export default function QuestionList() {
    const [loading, setLoading] = useState(false)
    const [questions, setQuestions] = useState()
    const [questionsF, setQuestionsF] = useState()
    const [question, setQuestion] = useState(false)
    const [modal, setModal] = useState(false)
    const [vue, setVue] = useState("base")
    const { t } = useTranslation('admin');


    const user = useSelector(state => state.user)

    useEffect(() => {
        if(vue==="base" && modal ===false){
            getQuestions()
        }
    }, [vue,modal])

    const columns = [
        // {
        //     name: 'Id',
        //     selector: 'id',
        //     grow: 1,
        //     sortable: true,
        // },
        {
            name: t("question.list.state"),
            selector: row => row.state.toString(),
            grow: 1,
            sortable: true,
        },
        {
            name: t("question.list.questEn"),
            selector: 'questionEn',
            grow: 8,
            sortable: true,
        },
        {
            name: t("question.list.questFr"),
            selector: 'questionFr',
            grow: 8,
            sortable: true,
        },
        {
            name: t("question.list.theme"),
            grow: 3,
            selector: row => [row.theme.themeFr, " " + row.theme.themeEn],
            sortable: true
        }
    ];

    return (
        <div className="fadeIn">
            {modal === true &&
                <ModalQuestion question={question} modal={modal} closeModal={closeModal} />
            }

            {vue === "base" &&
                <Fragment>
                    {loading === true ?
                        <div className="shadow p-3 mb-5 bg-white rounded col-10 offset-1">
                            <button className="btn btn-success mb-2" onClick={handleAdd}>{t("add")}</button>
                            <div className="col-md-6 offset-md-3">
                                <input type="text" className="form-control" onChange={filterQuest} placeholder={t("question.filter")}></input>
                            </div>

                            <DataTable
                                title="Questions"
                                columns={columns}
                                data={questionsF}
                                pagination={true}
                                highlightOnHover={true}
                                pointerOnHover={true}
                                onRowClicked={handleRowClick}
                                defaultSortField="state"
                            />
                        </div>
                        :
                        <Loading />
                    }
                </Fragment>
            }
            {vue === "add" &&
                <FormAdd user={user} goBack={goBack} />
            }

        </div>
    )

    async function getQuestions() {
        const response = await admin.getQuestions(user.apiToken)
        const data = await response.json()
        setLoading(true)
        setQuestions(data)
        setQuestionsF(data)
        return data
    }

    function closeModal() {
        setVue("base")
        setModal(false)
    }

    function handleRowClick(value) {
        setQuestion(value)
        setModal(true)
        setVue('modal')
    }

    function handleAdd() {
        setVue("add")
    }
    function goBack() {
        setVue("base")
    }

    function filterQuest(e) {
        let filter = questions.filter(question => {
            if (question.questionEn.includes(e.target.value) || question.questionFr.includes(e.target.value)) {
                return true
            }
            return false
        })
        setQuestionsF(filter)
    }
}



