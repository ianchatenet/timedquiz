import React, { Fragment } from "react";
import { useTranslation } from "react-i18next";



export default function FormUpdate(props) {
    const { t } = useTranslation('admin');

    let handleSubmit = props.handleSubmit
    let question = props.question

    return (

        <form onSubmit={handleSubmit}>
            <div className="form-group">
                <label >{t("question.update.question.label")}</label>
                <input type="text" className="form-control" id="questionFr" placeholder={t("question.update.question.placeHolderFr")}></input>
                <input type="text" className="form-control" id="questionEn" placeholder={t("question.update.question.placeHolderEn")}></input>
            </div>

            <div className="form-group">
                <label>{t("question.update.options.label")}</label>
                <div className="form-group">
                    <input type="text" className="form-control" id="Option1Fr" placeholder={t("question.update.options.option1Fr")}></input>
                    <input type="text" className="form-control" id="Option1En" placeholder={t("question.update.options.option1En")}></input>
                </div>

                {question.options.length > 1 &&

                    <div className="form-group">
                        <input type="text" className="form-control" id="Option2Fr" placeholder={t("question.update.options.option2Fr")}></input>
                        <input type="text" className="form-control" id="Option2En" placeholder={t("question.update.options.option2En")}></input>
                    </div>
                }
                {question.options.length > 2 &&
                    <div className="form-group">
                        <input type="text" className="form-control" id="Option3Fr" placeholder={t("question.update.options.option3Fr")}></input>
                        <input type="text" className="form-control" id="Option3En" placeholder={t("question.update.options.option3En")}></input>
                    </div>
                }
                {question.options.length > 3 &&

                    <div className="form-group">
                        <input type="text" className="form-control" id="Option4Fr" placeholder={t("question.update.options.option4Fr")}></input>
                        <input type="text" className="form-control" id="Option4En" placeholder={t("question.update.options.option4En")}></input>
                    </div>
                }

                <div className="form-group">
                    <label>{"State"}</label>
                    <select className="form-control" id="state">
                        <option value={true}>{t("question.update.state.true")}</option>
                        <option value={false}>{t("question.update.state.false")}</option>
                    </select>
                </div>

                <div className="form-group">
                    <label>{"Difficulty"}</label>
                    <select className="form-control" id="dif">
                        <option value={1}>1</option>
                        <option value={2}>2</option>
                        <option value={3}>3</option>
                    </select>
                </div>

            </div>
            <button type="submit" className="btn btn-primary">{t("question.update.submit")}</button>
        </form>
    )
}