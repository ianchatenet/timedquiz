import React, { useState, useEffect } from "react";
import { admin, mytholo } from "../../../api"
import Loading from "../../loader/Loading";
import { useTranslation } from "react-i18next";


export default function FormAdd(props) {
    const { t } = useTranslation('admin');

    const [optionNbr, setOptionNbr] = useState("1")
    const [errors, setErrors] = useState([])
    const [loading, setLoading] = useState(false)
    const [themes, setThemes] = useState()
    const [optCor, setOptCor] = useState([])
    const [langQuest, setLangQuest] = useState([])
    const [langOptE, setLangOptE] = useState([])
    const [checkTrue, setCheckTrue] = useState([])
    let user = props.user
    let goBack = props.goBack


    useEffect(() => {
        loadThemes();
    }, [])

    return (
        <div className="shadow p-3 mb-5 bg-white rounded col-10 offset-1">
            <button className="btn btn-primary" onClick={goBack}>{t('question.add.return')}</button>
            {loading == true ?
                <form onSubmit={handleSubmit}>
                    <h5>{t('question.add.title')}</h5>

                    <div className="form-group">
                        <label >{t('question.add.question.label')}</label>
                        {langQuest.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{error}</p>
                        )}
                        <input type="text" className="form-control" id="questionFr" placeholder={t('question.add.question.placeHolderFr')}></input>
                        <input type="text" className="form-control" id="questionEn" placeholder={t('question.add.question.placeHolderEn')}></input>
                    </div>

                    <div className="form-group">
                        <label>{t('question.add.options.nbr')}</label>
                        <select className="form-control" id="optionNbr" onChange={handleOptions}>
                            <option value={1}>1</option>
                            <option value={2}>2</option>
                            <option value={3}>3</option>
                            <option value={4}>4</option>
                        </select>
                    </div>
                    <div>
                        <label>{t('question.add.options.label')}</label>
                        {optionNbr > 0 &&

                            <div className="form-group">
                                {optCor.map((error, index) =>
                                    <p key={index} className="text-danger errorForm">{error}</p>
                                )}
                                {langOptE.map((error, index) =>
                                    <p key={index} className="text-danger errorForm">{error}</p>
                                )}
                                {checkTrue.map((theme, index) =>
                                    <option key={index} value={theme.id}>{theme.themeFr} {theme.themeEn}</option>
                                )}
                                <input type="text" className="form-control" id="Option1Fr" placeholder={t('question.add.options.option1Fr')}></input>
                                <input type="text" className="form-control" id="Option1En" placeholder={t('question.add.options.option1En')}></input>
                                <select className="form-control" id="Option1Cor" >
                                    <option value={false}>{t('question.add.options.false')}</option>
                                    <option value={true}>{t('question.add.options.true')}</option>
                                </select>
                            </div>
                        }
                        {optionNbr > 1 &&

                            <div className="form-group">
                                <input type="text" className="form-control" id="Option2Fr" placeholder={t('question.add.options.option2Fr')}></input>
                                <input type="text" className="form-control" id="Option2En" placeholder={t('question.add.options.option2En')}></input>
                                <select className="form-control" id="Option2Cor">
                                    <option value={false}>{t('question.add.options.false')}</option>
                                    <option value={true}>{t('question.add.options.true')}</option>
                                </select>
                            </div>
                        }
                        {optionNbr > 2 &&
                            <div className="form-group">
                                <input type="text" className="form-control" id="Option3Fr" placeholder={t('question.add.options.option3Fr')}></input>
                                <input type="text" className="form-control" id="Option3En" placeholder={t('question.add.options.option3En')}></input>
                                <select className="form-control" id="Option3Cor" >
                                    <option value={false}>{t('question.add.options.false')}</option>
                                    <option value={true}>{t('question.add.options.true')}</option>
                                </select>
                            </div>
                        }
                        {optionNbr > 3 &&
                            <div className="form-group">
                                <input type="text" className="form-control" id="Option4Fr" placeholder={t('question.add.options.option4Fr')}></input>
                                <input type="text" className="form-control" id="Option4En" placeholder={t('question.add.options.option4En')}></input>
                                <select className="form-control" id="Option4Cor" >
                                    <option value={false}>{t('question.add.options.false')}</option>
                                    <option value={true}>{t('question.add.options.true')}</option>
                                </select>
                            </div>
                        }
                    </div>

                    {themes &&
                        <div className="form-group">
                            <label>{t('question.add.themes.label')}</label>
                            <select className="form-control" id="theme">
                                {themes.map((theme, index) =>
                                    <option key={index} value={theme.id}>{theme.themeFr} {theme.themeEn}</option>
                                )}
                            </select>
                        </div>
                    }
                    <div className="form-group">
                        <label>{t("question.add.state.label")}</label>
                        <select className="form-control" id="state">
                            <option value={true}>{t("question.add.state.true")}</option>
                            <option value={false}>{t("question.add.state.false")}</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label>{t("question.add.dif.label")}</label>
                        <select className="form-control" id="dif">
                            <option value={1}>1</option>
                            <option value={2}>2</option>
                            <option value={3}>3</option>
                        </select>
                    </div>

                    {errors.length > 0 &&
                        <p className="text-danger">erreur</p>}
                    <button type="submit" className="btn btn-primary">{t("question.add.submit")}</button>
                </form>
                :
                <Loading />
            }
        </div >
    )
    async function loadThemes() {
        const response = await mytholo.getThemes()
        const data = await response.json()
        setLoading(true)
        setThemes(data)
    }


    function handleOptions(e) {
        setOptionNbr(parseInt(e.target.value))
    }
    function handleSubmit(e) {
        e.preventDefault()

        let array = checkForm(e)
        setErrors(array)
        let options = addOption(e)

        let question = {
            "questionFr": e.target.questionFr.value,
            "questionEn": e.target.questionEn.value,
            "options": options,
            "difficulty": e.target.dif.value,
            "state": JSON.parse(e.target.state.value),
            "theme": e.target.theme.value
        }
        if (array.length < 1) {
            admin.createQuestion(user, question)
            goBack()
        }
    }

    function addOption(e) {
        let options = []
        if (optionNbr > 0) {
            let option = {
                "optionFr": e.target.Option1Fr.value,
                "optionEn": e.target.Option1En.value,
                "isCorrect": e.target.Option1Cor.value
            }
            options.push(option)
        }
        if (optionNbr > 1) {
            let option = {
                "optionFr": e.target.Option2Fr.value,
                "optionEn": e.target.Option2En.value,
                "isCorrect": e.target.Option2Cor.value
            }
            options.push(option)
        }
        if (optionNbr > 2) {
            let option = {
                "optionFr": e.target.Option3Fr.value,
                "optionEn": e.target.Option3En.value,
                "isCorrect": e.target.Option3Cor.value
            }
            options.push(option)
        }
        if (optionNbr > 3) {
            let option = {
                "optionFr": e.target.Option4Fr.value,
                "optionEn": e.target.Option4En.value,
                "isCorrect": e.target.Option4Cor.value
            }
            options.push(option)
        }
        return options
    }

    function checkForm(e) {
        let form = e.target
        let array = []

        if (checkCor(e) === false) {
            setOptCor([t('question.add.errors.optCor')])
            array.push(t("question.add.errors.optCor"))
        }


        if (form.questionFr.value === "" || form.questionEn.value === "") {
            setLangQuest([t("question.add.errors.langQuest")])
            array.push(t("question.add.errors.langQuest"))
        }

        if (form.Option1Fr.value === "" || form.Option1En.value === "") {
            setLangOptE([t("question.add.errors.langOpt")])
            array.push(t("question.add.errors.langOpt"))
        }

        if (optionNbr > 1) {
            if (form.Option2Fr.value === "" || form.Option2En.value === "") array.push(t("question.add.errors.langOpt") + " option 2")
            if (form.Option2Cor.value === "true" && (form.Option2Cor.value === form.Option1Cor.value || form.Option2Cor.value === form.Option3Cor.value || form.Option2Cor.value === form.Option4Cor.value)) {
                setCheckTrue([t("question.add.errors.optCorOne")])
                array.push(t("question.add.errors.optCorOne"))
            }
        }

        if (optionNbr > 2) {
            if (form.Option3Fr.value === "" || form.Option3En.value === "") array.push(t("question.add.errors.langOpt") + " option 3")
            if (form.Option3Cor.value === "true" && (form.Option3Cor.value === form.Option1Cor.value || form.Option3Cor.value === form.Option2Cor.value || form.Option3Cor.value === form.Option4Cor.value)) {
                setCheckTrue([t("question.add.errors.optCorOne")])
                array.push(t("question.add.errors.optCorOne"))
            }

        }
        if (optionNbr > 3) {
            if (form.Option4Fr.value === "" || form.Option4En.value === "") array.push(t("question.add.errors.langOpt") + " option 4")
            if (form.Option4Cor.value === "true" && (form.Option4Cor.value === form.Option1Cor.value || form.Option4Cor.value === form.Option2Cor.value || form.Option4Cor.value === form.Option3Cor.value)) {
                setCheckTrue([t("question.add.errors.optCorOne")])
                array.push(t("question.add.errors.optCorOne"))
            }
        }
        return array
    }

    function checkCor(e) {
        let array = [e.target.Option1Cor.value]
        if (optionNbr > 1) array.push(e.target.Option2Cor.value)
        if (optionNbr > 2) array.push(e.target.Option3Cor.value)
        if (optionNbr > 3) array.push(e.target.Option4Cor.value)
        return array.includes("true")
    }
}
