import React, { useState, useEffect, Fragment } from "react"
import { useTranslation } from "react-i18next";
import Loading from "../../loader/Loading"
import { admin } from "../../../api"
import { useSelector } from 'react-redux'
import DataTable from 'react-data-table-component';
import ModalUser from './ModalUser'

export default function UsersList() {
    const { t } = useTranslation('admin');

    const [loading, setLoading] = useState(false)
    const [users, setUsers] = useState()
    const [usersF, setUsersF] = useState()
    const [clickedUser, setClickedUser] = useState()
    const [modal, setModal] = useState(false)


    const user = useSelector(state => state.user)

    useEffect(() => {
        if(modal===false)getUsers()
    }, [modal])


    const columns = [
        {
            name: t("user.username"),
            selector: "username",
            sortable: true,
        },
        {
            name: t("user.email"),
            selector: 'email',
            grow: 10,
            sortable: true,
        },
        {
            name: t("user.role"),
            selector: row => row.roles[0].slice(5).toLowerCase(),
            grow: 10,
            sortable: true,
        }
    ];

    return (
        <div className="fadeIn">
            {loading === true ?
                <Fragment>
                    {modal === true &&
                        <ModalUser user={clickedUser} modal={modal} closeModal={closeModal} />
                    }
                    <div className="shadow p-3 mb-5 bg-white rounded col-10 offset-1">
                        <div className="col-md-6 offset-md-3">
                            <input type="text" className="form-control" onChange={filterUser} placeholder={t("user.filter")}></input>
                        </div>
                        <DataTable
                            title={t("user.title")}
                            columns={columns}
                            data={usersF}
                            pagination={true}
                            highlightOnHover={true}
                            pointerOnHover={true}
                            onRowClicked={handleRowClick}
                            defaultSortField="state"
                        />
                    </div>
                </Fragment>
                :
                <Loading />
            }
        </div>
    )

    async function getUsers() {
        const response = await admin.getUsers(user.apiToken)
        const data = await response.json()
        setLoading(true)
        setUsers(data)
        setUsersF(data)
    }

    function handleRowClick(value) {
        setClickedUser(value)
        setModal(true)
    }

    function closeModal() {
        setModal(false)
    }

    function filterUser(e) {
        let filter = users.filter(user => {
            if (user.username.includes(e.target.value) || user.email.includes(e.target.value)) {
                return true
            }
            return false
        })
        setUsersF(filter)
    }
}
