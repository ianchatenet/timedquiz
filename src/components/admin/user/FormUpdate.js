import React from 'react'
import { useTranslation } from "react-i18next";

export default function FormUpdate(props) {
    const { t } = useTranslation('admin');

    let handleSubmit = props.handleSubmit

    return (
        <form onSubmit={handleSubmit} className="text-center">
            <div className="form-group">
                <label>{t("user.update.role")}</label>
                <select className="form-control" id="role">
                    <option value={"ROLE_ADMIN"}>ROLE_ADMIN</option>
                    <option value={"ROLE_MODO"}>ROLE_MODO</option>
                    <option value={"ROLE_USER"}>ROLE_USER</option>
                </select>
            </div>
            <button type="submit" className="btn btn-primary">{t("user.update.submit")}</button>

        </form>
    )
}