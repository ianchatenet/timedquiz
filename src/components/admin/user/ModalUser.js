import React, { useState, Fragment } from 'react'
import { useTranslation } from "react-i18next";
import Modal from 'react-modal';
import FormUpdate from './FormUpdate'
import {admin} from "../../../api"
import { useSelector } from 'react-redux'

export default function ModalUser(props) {
    const { t } = useTranslation('admin');

    let modal = props.modal
    let closeModal = props.closeModal
    let user = props.user

    const [vue, setVue] = useState("base")

    const currentUser = useSelector(state => state.user)

    const customStyles = {
        content: {
            top: '10%',
            left: '20%',
            right: '20%',
            width: "60%",
        }
    };
    Modal.setAppElement("#container")
    let lastActivity = new Date(user.lastActivity)
    let registerDate = new Date(user.createdAt)
    return (
        <Modal isOpen={modal} onRequestClose={closeModal} style={customStyles}>

            {vue === "base" &&
                <div className="text-center">
                    <h4>{t("user.modal.user")}: {user.username}</h4>
                    <p>{t("user.modal.email")}: {user.email}</p>
                    <p>{t("user.modal.role")}: {user.roles[0]}</p>
                    <p>{t("user.modal.regist")}: {registerDate.getDate() + "/" + registerDate.getMonth() + "/" + registerDate.getFullYear()}</p>
                    <p>{t("user.modal.lastAc")}: {lastActivity.getDate() + "/" + lastActivity.getMonth() + "/" + lastActivity.getFullYear()}</p>
                    <p>{t("user.modal.title")}: {user.title}</p>
                </div>
            }
            {vue === "update" &&
                <FormUpdate user={user} handleSubmit={handleSubmit}/>
            }

            <div className="text-center">
                {vue === "base" &&
                    <Fragment>
                        <button onClick={updateQuestion} className="btn btn-success">{t("user.modal.update")}</button>
                    </Fragment>
                }
                {vue === "update" && <button onClick={goBack} className="btn btn-success">{t("user.modal.return")}</button>}
                <button onClick={handleClose} className="btn btn-primary">{t("user.modal.close")}</button>

            </div>
        </Modal>
    )
    function handleClose() {
        closeModal()
        setVue("base")
    }

    function updateQuestion() {
        setVue("update")
    }

    function goBack() {
        setVue("base")
    }

    async function handleSubmit(e) {
        e.preventDefault()
        let userU = user
        userU.roles=[e.target.role.value]
        admin.updateUser(currentUser.apiToken, userU)
        handleClose()
    }

}