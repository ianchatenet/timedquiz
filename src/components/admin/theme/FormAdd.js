import React from "react"
import { admin } from "../../../api"
import { useState } from "react"
import { useTranslation } from "react-i18next";


export default function FormAdd(props) {
    const { t } = useTranslation('admin');

    let user = props.user
    let goBack = props.goBack

    const [errors, setErrors] = useState([])

    return (
        <div className="shadow p-3 mb-5 bg-white rounded col-10 offset-1">
            <button onClick={goBack} className="btn btn-primary">{t("theme.addForm.return")}</button>


            <form onSubmit={handleSubmit} className="text-center">
                <div className="form-group">
                    <label>{t("theme.addForm.label")}</label>
                    {errors.map((error, index) =>
                            <p key={index} className="text-danger errorForm">{error}</p>
                        )}
                    <input type="text" className="form-control" id="themeFr" placeholder={t("theme.addForm.placeholderFr")}></input>
                    <input type="text" className="form-control" id="themeEn" placeholder={t("theme.addForm.placeholderEn")}></input>
                </div>
                    <button type="submit" className="btn btn-primary">{t("theme.addForm.submit")}</button>
            </form>
        </div>
    )

    function handleSubmit(e) {
        e.preventDefault()
        if (checkErrors(e) === true) {
            let theme = {
                "themeFr": e.target.themeFr.value,
                "themeEn": e.target.themeEn.value
            }
            admin.createTheme(user.apiToken, theme)
            goBack()
        }

    }

    function checkErrors(e) {
        if (e.target.themeFr.value === "" || e.target.themeEn.value === "") {
            setErrors([t("theme.addForm.errors.allFields")])
            return false
        }
        setErrors([])
        return true
    }
}