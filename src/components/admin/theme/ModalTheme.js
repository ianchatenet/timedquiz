import React, { useState, Fragment } from 'react'
import Modal from 'react-modal';
import { useTranslation } from "react-i18next";
import FormUpdate from './FormUpdate'
import DeleteConf from '../../confirmation/DeleteConf'
import { admin } from "../../../api"
import { useSelector } from 'react-redux'


export default function ModalTheme(props) {
    const { t } = useTranslation('admin');

    let modal = props.modal
    let closeModal = props.closeModal
    let theme = props.theme

    const [vue, setVue] = useState("base")
    const [confirm, setConfirm] = useState(false)

    const currentUser = useSelector(state => state.user)

    const customStyles = {
        content: {
            top: '10%',
            left: '20%',
            right: '20%',
            width: "60%",
        }
    };
    Modal.setAppElement("#container")
    return (
        <Modal isOpen={modal} onRequestClose={closeModal} style={customStyles}>
            <DeleteConf modal={confirm} context="theme" deleteItem={deleteTheme} closeModal={closeConfirm} />
            {vue === "base" &&
                <div className="text-center">
                    <h4>{t("theme.modal.themeEn")} {theme.themeEn}</h4>
                    <h4>{t("theme.modal.themeFr")} {theme.themeFr}</h4>
                    <p>{theme.questions.length} {t("theme.modal.nbrQuest")}</p>
                </div>
            }
            {vue === "update" &&
                <FormUpdate theme={theme} handleSubmit={handleSubmit} />
            }

            <div className="text-center">
                {vue === "base" &&
                    <Fragment>
                        <button onClick={updateQuestion} className="btn btn-success">{t("theme.modal.update")}</button>
                        <button onClick={handleConfirm} className="btn btn-danger">{t("theme.modal.delete")}</button>
                    </Fragment>
                }
                {vue === "update" &&
                    <button onClick={goBack} className="btn btn-success">{t('theme.modal.return')}</button>}
                <button onClick={handleClose} className="btn btn-primary">{t("theme.modal.close")}</button>

            </div>
        </Modal>
    )

    function handleClose() {
        closeModal()
        setVue("base")
    }

    function updateQuestion() {
        setVue("update")
    }

    function handleConfirm() {
        setConfirm(true)
    }
    function closeConfirm(){
        setConfirm(false)
    }
    function deleteTheme() {
        if (theme.questions.length < 1) {
            admin.deleteTheme(currentUser.apiToken, theme)
            closeModal()
        }else{
            alert("nope")
        }
    }

    function goBack() {
        setVue("base")
    }

    async function handleSubmit(e) {
        e.preventDefault()
        let themeU = theme
        if (e.target.themeFr.value) themeU.themeFr = e.target.themeFr.value
        if (e.target.themeEn.value) themeU.themeEn = e.target.themeEn.value
        admin.updateTheme(currentUser.apiToken, themeU)
        handleClose()
        setVue("base")
    }
}