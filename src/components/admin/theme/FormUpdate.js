import React from 'react'
import { useTranslation } from "react-i18next";


export default function FormUpdate(props) {
    const { t } = useTranslation('admin');

    let theme = props.theme
    let handleSubmit = props.handleSubmit
    return (
        <form onSubmit={handleSubmit} className="text-center">
            <div className="form-group">
                <label>{t("theme.update.themeLabel")}</label>
                <input type="text" className="form-control" id="themeFr" placeholder={t("theme.update.themeFrPh")}></input>
                <input type="text" className="form-control" id="themeEn" placeholder={t("theme.update.themeEnPh")}></input>
            </div>

            <button type="submit" className="btn btn-primary">{t("theme.update.submit")}</button>
        </form>
    )
}