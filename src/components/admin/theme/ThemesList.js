import React, { useState, useEffect, Fragment } from "react"
import { useTranslation } from "react-i18next";
import Loading from '../../loader/Loading.js';
import { admin } from '../../../api';
import { useSelector } from 'react-redux'
import DataTable from 'react-data-table-component';
import ModalTheme from './ModalTheme'
import FormAdd from './FormAdd';


export default function ThemesList() {
    const { t } = useTranslation('admin');

    const [loading, setLoading] = useState(false)
    const [themes, setThemes] = useState()
    const [themesF, setThemesF] = useState()
    const [modal, setModal] = useState(false)
    const [themeClicked, setThemeClicked] = useState()
    const [vue, setVue] = useState("base")

    const user = useSelector(state => state.user)

    useEffect(() => {
        if(vue ==="base" && modal===false){
            getThemes()
        }
    }, [vue,modal])

    const columns = [
        // {
        //     name: 'Id',
        //     selector: 'id',
        //     sortable: true,
        // },
        {
            name: t("theme.themeEn"),
            selector: "themeEn",
            sortable: true,
        },
        {
            name: t("theme.themeFr"),
            selector: 'themeFr',
            sortable: true,
        },
        {
            name: t("theme.questNbr"),
            selector:row=> row.questions.length,
            sortable: true
        }
    ];

    return (

        <div className="fadeIn">
            {modal === true &&
                <ModalTheme theme={themeClicked} modal={modal} closeModal={closeModal} />
            }
            {vue === "base" &&
                <Fragment>
                    {loading === true ?
                        <div className="shadow p-3 mb-5 bg-white rounded col-10 offset-1">
                            <button className="btn btn-success mb-2" onClick={handleAdd}>{t("theme.add")}</button>
                            <div className="col-md-6 offset-md-3">
                                <input type="text" className="form-control" onChange={filterThemes} placeholder={t("question.filter")}></input>
                            </div>
                            <DataTable
                                title={t("theme.title")}
                                columns={columns}
                                data={themesF}
                                pagination={true}
                                highlightOnHover={true}
                                pointerOnHover={true}
                                onRowClicked={handleRowClick}
                                defaultSortField="state"
                            />
                        </div>
                        :
                        <Loading />
                    }
                </Fragment>
            }

            {vue === "add" &&
                <FormAdd user={user} goBack={goBack} />
            }
        </div>
    )

    async function getThemes() {
        const response = await admin.getThemes(user.apiToken)
        const data = await response.json()
        setLoading(true)
        setThemes(data)
        setThemesF(data)
    }
    function handleAdd() {
        setVue("add")
    }
    function handleRowClick(value) {
        setThemeClicked(value)
        setModal(true)
        setVue("modal")
    }
    function closeModal() {
        setModal(false)
        setVue("base")
    }
    function goBack() {
        setVue("base")
    }

    function filterThemes(e) {
        let filter = themes.filter(theme => {
            if (theme.themeEn.includes(e.target.value) || theme.themeFr.includes(e.target.value)) {
                return true
            }
            return false
        })
        setThemesF(filter)
    }
}

