import React from 'react'

export default function Loading() {

    return (
        <div className="container text-center">
            <div className="spinner-border text-primary" >
                <span className="sr-only"></span>
            </div>
        </div>
    )
}