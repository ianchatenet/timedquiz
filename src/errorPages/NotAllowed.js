import React, { Fragment } from "react"
import { Redirect } from "react-router-dom";
import { useState } from "react";
import { useTranslation } from "react-i18next";

export default function NotAllowed() {

    const { t } = useTranslation('ePages');
    const [vue, setVue] = useState()

    return (
        <div className="text-center">
            {vue === "home" &&
                <Redirect to="/" />
            }
            <div className="col-12 shadow p-3 mb-5 bg-white rounded titleBar fadeIn">
                <h3>{t("403.title")}</h3>
            </div>
            <div>
                <h3>{t("403.subTitle")}</h3>
                <button className="btn btn-primary" onClick={handleClick}>{t("403.button")}</button>
            </div>
        </div>
    )

    function handleClick() {
        setVue("home")
    }
}