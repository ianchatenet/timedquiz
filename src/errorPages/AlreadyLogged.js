import React, { Fragment } from "react"
import { Redirect } from "react-router-dom";
import { useState } from "react";
import { useTranslation } from "react-i18next";


export default function AlreadyLogged() {

    const { t } = useTranslation('ePages');
    const [vue, setVue] = useState()

    return (
        <div className="text-center">
            {vue === "home" &&
                <Redirect to="/" />
            }
            <div className="col-12 shadow p-3 mb-5 bg-white rounded titleBar fadeIn">
                <h3>{t("logged.title")}</h3>
            </div>
            <div>
                <h3>{t("logged.subTitle")}</h3>
                <button className="btn btn-primary" onClick={handleClick}>{t("logged.button")}</button>
            </div>
        </div>
    )

    function handleClick() {
        setVue("home")
    }
}